#ifndef VARS_INCLUDED
#define VARS_INCLUDED

using namespace std;

float major_grade_points;
float max_major_grade_points;
float minor_grade_points;
float max_minor_grade_points;
float other_grade_points;
float max_other_grade_points;

float getMajorGradePoints();
float getMinorGradePoints();
float getOtherGradePoints();

bool running = true;
string answer;

#endif // VARS_INCLUDED

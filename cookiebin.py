#!/usr/bin/env python
## Really basic 'pastebin' script
## Made by Emma Jones (AwwCookies)

import cgi, cgitb, sqlite3, sys
SCRIPT_URL = ""
conn = sqlite3.connect("cookiebin.db")
cur = conn.cursor()
cur.execute("CREATE TABLE IF NOT EXISTS cookiebin (id INTEGER PRIMARY KEY, input TEXT)")

data = cgi.FieldStorage()

if data.getvalue("id"):
    print "Content-type:text/html\r\n\r\n"
    cur.execute("SELECT * FROM cookiebin")
    for row in cur:
        if str(row[0]) == str(data.getvalue("id")): 
            print row[1]
else:
    if data.getvalue("input"):
        text = data.getvalue("input")
    else:
        print "Content-type:text/html\r\n\r\n"
        print "ERROR: NO INPUT."
        sys.exit()

cur.execute("INSERT INTO cookiebin (input) VALUES ('%s')" % text)
print "Content-type:text/html\r\n\r\n"
print SCRIPT_URL + str(cur.lastrowid)
conn.commit()
conn.close()

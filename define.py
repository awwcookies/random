###################################################
__module_author__ = 'Aww'
__module_name__ = 'Define'
__module_version__ = '0.0.1'
__module_description__ = 'Dict for XChat'
####################################################
import xchat
from wordnik import *
apiUrl = 'http://api.wordnik.com/v4'
apiKey = ''
def define(word, word_eol, userdata):
  try:
		client = swagger.ApiClient(apiKey, apiUrl)
		wordApi = WordApi.WordApi(client)
		definitions = wordApi.getDefinitions(word[1].replace(" ", "+"),limit=1)
		xchat.emit_print("Channel Message", "&define", word[1] + ": " + definitions[0].text, "")
	except:
		xchat.emit_print("Channel Message", "&define", "Error.", "")

xchat.hook_command("define", define)

/*

@auth AwwCookies

This program calulates the grade in a students class
The student can calculates what they have to make on
the next quiz or test to get 'x' grade.

*/

#include <iostream>
#include <string>
#include "vars.h"
int main() {
    do {
        cout << "Welcome to the Grade Calculator!" << endl;
        cout << "To begin enter your major grade score and max score" << endl;
        cout << "Example: 100 200" << endl;

        cin >> major_grade_points;
        cin >> max_major_grade_points;

        cout << "You have " << getMajorGradePoints() << " major grade points" << endl;
        cout << "Now enter your minor grade score and max score" << endl;

        cin >> minor_grade_points;
        cin >> max_minor_grade_points;

        cout << "You have " << getMinorGradePoints() << " minor grade points" << endl;
        cout << "Now enter your other grade score and max score" << endl;

        cin >> other_grade_points;
        cin >> max_other_grade_points;

        cout << "You have " << getOtherGradePoints() << " other grade points" << endl;
        cout << "Your grade in that class is about a " << getMajorGradePoints() + getMinorGradePoints() + getOtherGradePoints() << endl;
        cout << "WARNING: This grade is based on a 60, 30, 10 grading system." << endl;
        cout << "Would you like to run this again? (Yes or No)" << endl;
        cin >> answer;

        if(answer == "No") {
            running = !running;
        }

    } while(running);
    return 0;
}

float getMajorGradePoints() {
    return major_grade_points/max_major_grade_points * 60;
}
float getMinorGradePoints() {
    return minor_grade_points/max_minor_grade_points * 30;
}
float getOtherGradePoints() {
    return other_grade_points/max_other_grade_points * 10;
}
